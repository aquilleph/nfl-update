from email.mime.text import MIMEText
from time import sleep
import unicodedata
import requests
import smtplib
import urllib2 
import nflgame

casson_uid = '16C2927689C6475A9324D18258B109CB'
clay_uid   = 'EA8A95C14163411691635752208CA2EF'



yds = 0
rec = 0
tds = 0

def main():
	ids   = {'Casson': '16C2927689C6475A9324D18258B109CB', 'Clay': 'EA8A95C14163411691635752208CA2EF'}  
	users = {'Casson': {'QB': ['P.Manning', 0, 0, 0], 'RB': ['R.Rice', 0, 0, 0, 0, 0, 0]}, 'Clay': {'WR': ['W.Welker', 0, 0, 0]}}

	while True:
		getStats(users, ids)
		


def getStats(users, ids):

	week1 = nflgame.games(2013, 1)
	players = nflgame.combine(week1)

	for user in users:
		uid = ids[user]

		for position in users[user]:
			if position == 'WR':
				player = users[user]['WR']
				receivers = players.receiving()

				for p in receivers:
					if player[0] in p.name and not p.receiving_rec == player[1]:
						message = player[0] + ' caught a pass for ' + str(p.receiving_yds - player[2]) + ' yds'
						player[1] = p.receiving_rec
						player[2] = p.receiving_yds

						if not p.receiving_tds == player[3]:
							message += ' and a touchdown'
							player[3] = p.receiving_tds

						sendRequest(uid, message)


			if position == 'QB':
				player = users[user]['QB']
				qbs = players.passing()

				for p in qbs:
					if player[0] in p.name and not p.passing_cmp == player[1]:
						message = player[0] + ' completed a pass for ' + str(p.passing_yds - player[2]) + ' yds'
						player[1] = p.passing_cmp
						player[2] = p.passing_yds

						if not p.passing_tds == player[3]:
							message += ' and a touchdown'
							player[3] = p.passing_tds

						sendRequest(uid, message)

			if position == 'RB':
				player = users[user]['RB']
				rbs = players.rushing()

				for p in rbs:
					if player[0] in p.name and not p.rushing_yds == player[2]:
						message = player[0] + ' ran for ' + str(p.rushing_yds - player[2]) + ' yds'
						player[2] = p.rushing_yds

						if not p.rushing_tds == player[3]:
							message += ' and a touchdown'
							player[3] = p.rushing_tds

						sendRequest(uid, message)


				receivers = players.receiving()

				for p in receivers:
					if player[0] in p.name and not p.receiving_rec == player[4]:
						message = player[0] + ' caught a pass for ' + str(p.receiving_yds - player[5]) + ' yds'
						player[4] = p.receiving_rec
						player[5] = p.receiving_yds

						if not p.receiving_tds == player[6]:
							message += ' and a touchdown'
							player[6] = p.receiving_tds

						sendRequest(uid, message)



def sendRequest(uid, message):
	subject = 'FFB Update'
	url     = 'http://games.espn.go.com/ffl/mobile/web/standings?leagueId=52633&seasonId=2013&wjb'

	try:
		requests.post(
            "https://api.omniv.us:8000/api/1/bursts/add", {
	            "recipient": uid,
	            "sender"   : subject,
	            "message"  : message,
	            "url"      : url
            }, verify = False
		)

	except Exception, e:
    		print e

if __name__ == '__main__':
	main()